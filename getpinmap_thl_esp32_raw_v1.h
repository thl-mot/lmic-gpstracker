/*
 * getpinmapthllesp32rawv1.h
 *
 *  Created on: 24.09.2019
 *      Author: thomas
 */

#ifndef GETPINMAP_THL_ESP32_RAW_V1_H_
#define GETPINMAP_THL_ESP32_RAW_V1_H_

/*

Module:  arduino_lmic_hal_boards.h

Function:
        Arduino-LMIC C++ HAL pinmaps for various boards

Copyright & License:
        See accompanying LICENSE file.

Author:
        Terry Moore, MCCI       November 2018

*/

#pragma once

#ifndef _arduino_lmic_hal_boards_h_
# define _arduino_lmic_hal_boards_h_

#include "arduino_lmic_hal_configuration.h"

namespace Arduino_LMIC {

	const HalPinmap_t* GetPinmap_thl_esp32_raw_v1 ();

}

#endif /* _arduino_lmic_hal_boards_h_ */

#endif /* GETPINMAP_THL_ESP32_RAW_V1_H_ */
