#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2019-09-26 22:05:22

#include "Arduino.h"
#include <Arduino.h>
#include <arduino_lmic.h>
#include <arduino_lmic_hal_boards.h>
#include <arduino_lmic_lorawan_compliance.h>
#include "arduino_lmic_hal_configuration.h"
#include <hal/hal.h>
#include <LoRaConfig.h>
#include <Nmea.h>
#include <SPI.h>

void log_assertion(const char *pMessage, uint16_t line) ;
void myEventCb(void *pUserData, ev_t ev) ;
void eventPrintAll(void) ;
bool eventPrintOne(void) ;
static void eventjob_cb(osjob_t *j) ;
const char *getSfName(rps_t rps) ;
const char *getBwName(rps_t rps) ;
const char *getCrName(rps_t rps) ;
const char *getCrcName(rps_t rps) ;
void printHex2(unsigned v) ;
void printHex4(unsigned v) ;
void printSpace(void) ;
void printFreq(u4_t freq) ;
void printRps(rps_t rps) ;
void printTxChnl(u1_t txChnl) ;
void printDatarate(u1_t datarate) ;
void printTxrxflags(u1_t txrxFlags) ;
void printSaveIrqFlags(u1_t saveIrqFlags) ;
void printNl(void) ;
void myRxMessageCb(void *pUserData, uint8_t port, const uint8_t *pMessage, 		size_t nMessage) ;
void do_send(osjob_t* j) ;
void sendComplete(void *pUserData, int fSuccess) ;
void myFail(const char *pMessage) ;
void setup_printSignOnDashLine(void) ;
static constexpr const char *filebasename2(const char *s, const char *p) ;
static constexpr const char *filebasename(const char *s) ;
void printVersionFragment(char sep, uint8_t v) ;
void printVersion(uint32_t v) ;
void setup_printSignOn() ;
void setupForNetwork(bool preJoin) ;
void setup_calibrateSystemClock(void) ;
void setup() ;
void loop() ;


#include "compliance-otaa-halconfig.ino"

#endif
